# Article Blog

A blog to handle articles
 * CRUD articles
 * Marked them as archive
 * Set related articles

## Installation

1. You will need php, composer and mysql to run the project

2. Install Dependencies
```bash
composer install
```

3. Configure database connection in config/app.php (copy file from config/app.default.php) line 251
```bash
'host' => 'localhost',
'username' => 'root',
'password' => '',
'database' => 'cake_blog',
```

4. Create database
```bash
create database YOUR_DATABASE_NAME;
```

5. Run migrations
```bash
bin/cake migrations migrate
```

6. Run app with built-in webserver or apache
```bash
bin/cake server -p 8765
```
Then visit `http://localhost:8765` to see the welcome page.