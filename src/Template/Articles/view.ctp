    <?php
        $archiveError = "";
        $archiveMsg = "";
        if ($article->archive) {
            $archiveError = "archive-article";
            $archiveMsg = "*** Article has been marked as Archive";
        }
    ?>

    <?php $archive = $article->archive ? 'false' : 'true'; ?>
    <?php $archiveText = $article->archive ? 'Remove Archive' : 'Mark as Archive'; ?>
    <?= $this->Html->link($archiveText, ['action' => 'patch', $article->id, '?' => ["archive" => $archive]]) ?>
<h1 class="<?php echo $archiveError; ?>"><?= h($article->title) ?></h1>
<p><?= h($article->references) ?></p>
<p><?= h($article->body) ?></p>
<?= $this->Form->control('relatedArticle', [
        'disabled' => $disableCheckbox,
        'type' => 'select',
        'multiple' => true,
        'options' => $relatedOptions,
        'default' => $selectedOptions
    ]);
?>
<p><small>Created: <?= $article->created->format(DATE_RFC850) ?></small></p>
<p class="<?php echo $archiveError; ?>"><small><?= $archiveMsg ?></small></p>