<h1>Blog articles</h1>
<p><?= $this->Html->link('Add Article', ['action' => 'add']) ?></p>
<table>
    <tr>
        <th>Id</th>
        <th>Title</th>
        <th>Reference</th>
        <th>Created</th>
        <th>Actions</th>
    </tr>

    <?php foreach ($articles as $article): ?>
    <?php
        $archiveError = "";
        if ($article->archive) {
            $archiveError = "archive-article";
        }
    ?>
    <tr class="<?php echo $archiveError; ?>">
        <td><?= $article->id ?></td>
        <td>
            <?= $this->Html->link($article->title, ['action' => 'view', $article->id]) ?>
        </td>
        <td>
            <?= $article->reference ?>
        </td>
        <td>
            <?= $article->created->format(DATE_RFC850) ?>
        </td>
        <td>
            <?= $this->Form->postLink(
                'Delete',
                ['action' => 'delete', $article->id],
                ['confirm' => 'Are you sure?'])
            ?>
             |
            <?= $this->Html->link('Edit', ['action' => 'edit', $article->id]) ?>
            |
            <?php $archive = $article->archive ? 'false' : 'true'; ?>
            <?php $archiveText = $article->archive ? 'Remove Archive' : 'Mark as Archive'; ?>
            <?= $this->Html->link($archiveText, ['action' => 'patch', $article->id, '?' => ["archive" => $archive]]) ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>