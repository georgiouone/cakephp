<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use App\Controller\AppController;

class ArticlesController extends AppController
{

    /**
     * Initialize controller
     *
     * @throws \Exception
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Flash'); // Include the FlashComponent
    }

    /**
     * Display all articles
     */
    public function index()
    {
        $this->set('articles', $this->Articles->find('all'));
    }

    /**
     * View specific article including related articles
     * @param $id
     */
    public function view($id)
    {
        $article = $this->Articles->get($id);
        $disableCheckbox = true;
        $this->_prepareRelatedArticleCheckbox($article, $disableCheckbox);
        $this->set(compact('article'));
    }

    /**
     * Create new article
     * @return \Cake\Http\Response|null
     */
    public function add()
    {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {

            $article = $this->Articles->patchEntity($article, $this->request->getData());

            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                //Parse validation errors and present them
                $this->_parseValidationErrors($article);
            }

        }
        $this->set('article', $article);
    }

    /**
     * Edit specific article including related articles
     * @param null $id
     * @return \Cake\Http\Response|null
     */
    public function edit($id = null)
    {

        $article = $this->Articles->get($id);
        $disableCheckbox = false;
        $this->_prepareRelatedArticleCheckbox($article, $disableCheckbox);

        if ($this->request->is(['post', 'put'])) {

            //add related article IDs on the post request as string instead of array
            $postData = $this->request->getData();
            $postData['related'] = $this->_preparePostRelatedArticles($postData);

            $this->Articles->patchEntity($article, $postData);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Your article has been updated.'));
                return $this->redirect(['action' => 'index']);
            } else {
                //Parse validation errors and present them
                $this->_parseValidationErrors($article);
            }

        }

        $this->set('article', $article);
    }

    /**
     * Delete  specific article
     * @param $id
     * @return \Cake\Http\Response|null
     */
    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);

        $article = $this->Articles->get($id);
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('The article with id: {0} has been deleted.', h($id)));
            return $this->redirect(['action' => 'index']);
        }

        //Parse validation errors and present them
        $this->_parseValidationErrors($article);

    }

    /**
     * Mark specific article as archive or not
     * @param null $id
     */
    public function patch($id = null)
    {

        $queryParameters = $this->request->getQueryParams();

        //convert archive input to boolean
        $archiveValue = $queryParameters['archive'];
        if ($archiveValue === 'true') {
            $mark = 'marked';
            $archiveBoolean = true;
        } else {
            $mark = 'unmarked';
            $archiveBoolean = false;
        }

        $article = $this->Articles->get($id);
        $article->archive = $archiveBoolean;

        if ($this->Articles->save($article)) {
                $this->Flash->success(__("Article has been $mark as Archive."));

                $disableCheckbox = true;
                $this->_prepareRelatedArticleCheckbox($article, $disableCheckbox);
                $this->set('article', $article);
                return $this->redirect(['action' => 'index']);
        } else {
            //Parse validation errors and present them
            $this->_parseValidationErrors($article);
        }
    }

    /**
     * Parse validation errors and present them
     * @param $article
     */
    private function _parseValidationErrors($article) {
        if ($article->errors()) {
            $errorList = [];
            foreach ($article->errors() as $categoryName => $categoryList) {
                foreach ($categoryList as $key => $value) {
                    $errorList[] = $categoryName . ': ' . $value;
                }
            }

            $this->Flash->error(__("Please fix the following error(s):\n \r".implode("\n \r", $errorList)));
        }
    }

    /**
     * Prepare related articles including the marked as related. Also disable the checkbox if needed
     * @param $article
     * @param $disableCheckbox
     */
    private function _prepareRelatedArticleCheckbox($article, $disableCheckbox)  {

        $articles = TableRegistry::get('Articles');
        $articlesOp = $articles->find()->select(['id', 'title'])->where(['id !=' => $article->id]);
        $relatedOptions = [];
        foreach ($articlesOp as $artValue) {
            $relatedOptions[$artValue->id] = $artValue->title;
        };

        $this->set('disableCheckbox', $disableCheckbox);
        $this->set('relatedOptions', $relatedOptions);

        $relatedList = explode(',', $article->related);
        $selectedOptions = [];
        forEach($relatedList as $related) {
            $selectedOptions[] = $related;
        }
        $this->set('selectedOptions', $selectedOptions);
    }

    /**
     * Prepare the related articles from post request
     * Convert related article IDs on the post request as string instead of array
     * if is empty return null
     * @param $postData
     * @return null|string
     */
    private function _preparePostRelatedArticles($postData) {

        if ($postData['relatedArticle']) {

            $related = implode(',', $postData['relatedArticle']);
            unset($postData['relatedArticle']);
            return $related;
        } else {
            return null;
        }
    }

}