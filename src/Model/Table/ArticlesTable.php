<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ArticlesTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
    }

    /**
     * Add validation for article table
     *
     * @param Validator $validator
     * @return Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('title')
            ->requirePresence('title')
            ->notEmpty('body')
            ->requirePresence('body');

        //extend validation of article to verify if reference is unique
        $validator->add(
            'reference',
            ['unique' => [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => 'The reference exists in another article']
            ]
        );

        return $validator;
    }
}